CREATE DATABASE booking_db;
use booking_db;

CREATE TABLE City (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    city_name VARCHAR(30) NOT NULL
);

CREATE TABLE ConfortLevel (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    confort_level INTEGER NOT NULL
);

CREATE TABLE Facility (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    facility_name VARCHAR(30) NOT NULL,
    conf_level_id INTEGER NOT NULL,
    CONSTRAINT facility_conf_level FOREIGN KEY (conf_level_id) 
    REFERENCES ConfortLevel (id)
    ON DELETE CASCADE
);

CREATE TABLE Hotel (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    hotel_name VARCHAR(30) NOT NULL,
    city_id INTEGER NOT NULL,
    stars INTEGER NOT NULL,
    CONSTRAINT hotel_city_id FOREIGN KEY (city_id) 
    REFERENCES City (id)
    ON DELETE CASCADE
);

CREATE TABLE HotelConfortLevel (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    conf_level_id INTEGER NOT NULL,
    hotel_id INTEGER NOT NULL,
    CONSTRAINT hotel_conf_level FOREIGN KEY (conf_level_id) 
    REFERENCES ConfortLevel (id)
    ON DELETE CASCADE,
    CONSTRAINT conf_level_hotel FOREIGN KEY (hotel_id) 
    REFERENCES Hotel (id)
    ON DELETE CASCADE,
    CONSTRAINT Hotel_Confort UNIQUE (hotel_id, conf_level_id)
);

CREATE TABLE Room (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    room_floor INTEGER NOT NULL,
    hotel_id INTEGER NOT NULL,
    max_pers INTEGER NOT NULL,
    description VARCHAR(100) NOT NULL,
    CONSTRAINT room_hotel_id FOREIGN KEY (hotel_id) 
    REFERENCES Hotel (id)
    ON DELETE CASCADE
);

CREATE TABLE Guest (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    email VARCHAR(30) NOT NULL UNIQUE
);

CREATE TABLE Reservation (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    room_id INTEGER NOT NULL,
    check_in_date DATETIME NOT NULL,
    check_out_date DATETIME NOT NULL,
    CONSTRAINT reserv_room_id FOREIGN KEY (room_id) 
    REFERENCES Room (id)
    ON DELETE CASCADE
);


